let _locale = GtkMain.Main.init ()

(* Status bar to print game informations*)
let status = GMisc.statusbar ()
let st = status#new_context ~name:"st"

(* Main window *)
let window = GWindow.window ~width:320 ~height:240
    ~title:"Jeu des manchots" ()

(* use these functions to display messages in the status bar *)
let st_push s = ignore (st#pop(); st#push s)
let st_flash ?delay:(delay=5000) s = ignore ((* st#pop(); *) st#flash ~delay s)
let st_pop () = ignore (st#pop())

let get_gs() = match !Client_state.gs with
  | Some (state) -> state
  | None -> failwith "uninitialized game state"

let load_game = ref (fun () -> ())
